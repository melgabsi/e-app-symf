<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EappController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('eapp/index.html.twig');
    }
    /**
     * @Route("/{word}", name="resultpage")
     */
    public function resultAction($word, Request $request)
    {
	
	shell_exec('python /home/melgabsi/Bureau/e-app/src/AppBundle/Controller/python.py  '.$word);
	$just = realpath('def/'.$word.'.txt');
	$def= nl2br(file_get_contents($just));     
	$def = mb_convert_encoding($def, 'HTML-ENTITIES', "utf-8");   
	$just1 = realpath('info/'.$word.'.txt');
	$asso= file_get_contents($just1);
	$asso= mb_convert_encoding($asso, 'HTML-ENTITIES', "iso-8859-1");   
	// replace this example code with whatever you need
        return $this->render('eapp/result.html.twig',array('word' => $word, 'def' => $def, 'asso' => $asso));
    }
}
