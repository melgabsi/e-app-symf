#!/usr/bin/env python 
#-*- coding: utf-8 -*-
import urllib
import urllib2
import os.path
from datetime import date
import datetime
import os
import sys


#------------------------------------------------------------------------------------------------
#----------------------------------------------Intro---------------------------------------------
#------------------------------------------------------------------------------------------------

save_path =  os.path.dirname(os.path.realpath("apple-touch-icon.png"))
save_path = save_path+'/data'
if not os.path.exists(save_path):
    os.makedirs(save_path)

#recupere premier parametre

mot = sys.argv[1]
mot1 = mot
mot = mot.replace("à", "%E0")
mot = mot.replace("á", "%E1")
mot = mot.replace("â", "%E2")
mot = mot.replace("ã", "%E3")
mot = mot.replace("ä", "%E4")
mot = mot.replace("å", "%E5")
mot = mot.replace("æ", "%E6")
mot = mot.replace("ç", "%E7")
mot = mot.replace("è", "%E8")
mot = mot.replace("é", "%E9")

mot = mot.replace("ð", "%F0")
mot = mot.replace("ñ", "%F1")
mot = mot.replace("ò", "%F2")
mot = mot.replace("ó", "%F3")
mot = mot.replace("ô", "%F4")
mot = mot.replace("õ", "%F5")
mot = mot.replace("ö", "%F6")
mot = mot.replace("÷", "%F7")
mot = mot.replace("ø", "%F8")
mot = mot.replace("ù", "%F9")


completeName = os.path.join(save_path,mot1+".txt")
completeName_data = os.path.join(save_path,mot1+"_data.txt")
# probleme : il faut augmenter le nbr des visite en mode update sans recreation fichier.

#------------------------------------------------------------------------------------------------
#--------------------------------------Creation des fonctions------------------------------------
#------------------------------------------------------------------------------------------------

#def fonction creation/update fichier
checkexist= '<div class="jdm-warning"><br>Le terme '

def makefile(nbr,exp):
	today = date.today()+ datetime.timedelta(exp)
	link = 'http://www.jeuxdemots.org/rezo-dump.php?gotermsubmit=Chercher&gotermrel='+mot+'&rel='
	codeSrc = urllib.urlopen(link).read()
	if checkexist in codeSrc:
#		print 'Le terme '+mot1+" n'existe pas !"
		sys.exit()
	else:
		string1="<CODE>"
		string2='</CODE>'
		pos1=codeSrc.find(string1)+len(string1)
		pos2= codeSrc.find(string2)	
		sousChaine = codeSrc[pos1:pos2]
		fichier1 = open(completeName_data ,"w")
		fichier1.write(str(today))
		fichier1.write('\n')	
		fichier1.write(str(nbr))
		fichier1.close()
		fichier = open(completeName, "w")
		fichier.write(sousChaine)
		fichier.close()	

def newdata(nbr,exp):
	fichier1 = open(completeName_data ,"w")
	fichier1.write(exp)
	fichier1.write(str(nbr))
	fichier1.close()
def newdata1(nbr,exp):
	fichier1 = open(completeName_data ,"w")
	fichier1.write(exp)
	fichier1.write('\n')	
	fichier1.write(str(nbr))
	fichier1.close()
#------------------------------------------------------------------------------------------------
#--------------------------------------------Code-------------------------------------------
#------------------------------------------------------------------------------------------------

inorout = ""

# si le fichier existe

if os.path.isfile(completeName):
# nombre de fois visiter 
	with open(completeName_data, 'r') as fin:
		nbr = fin.readlines()[1:2]
	#	print ("the nbre of visits:"+str(nbr))
		newnbr = '0'
		for l in str(nbr):
			if l in ('1','2','3','4','5','6','7','8','9','0'):
				newnbr = newnbr+l
		nbr = int(str(newnbr)) + 1
# tester selon la date d'expiration 
	with open(completeName_data, 'r') as fin:
		datef = fin.readline()
		if nbr > 99 :
			exp = 7
		else :
			exp = 15
# si il faut faire un update
		if str(date.today())> datef:
		#	print ("file update required")
			makefile(str(nbr),exp)
		elif nbr == 100: 
	#		print ("this is now a frequently visited file")
			datef = datetime.datetime.strptime(datef, '%Y-%m-%d\n').date()- datetime.timedelta(8)
			newdata1(str(nbr),str(datef))
			sys.exit()	
		else :
			newdata(str(nbr),str(datef))	
			sys.exit()
	#		print ("update nbre of visits")
else:
	makefile(1,15)
#	print ("first file creation")

#*******************************************************************************************************************
relationids = []
dicelement = {0:'0'}
dicrelation  = {0:'0'}
dicdefrelation = {0:'0'}
dicrassociation = {}
#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------lire argument ( mot rechercher )-------------------------------
#-----------------------------------------------------------------------------------------------------------------------

mot =mot1
#-----------------------------------------------------------------------------------------------------------------------
#------------------------------trouver fichier contenant resultat----------------------------------
#-----------------------------------------------------------------------------------------------------------------------

save_path =  os.path.dirname(os.path.realpath("python.py"))
save_path_data = save_path+'/data'
completeName = os.path.join(save_path_data,mot+".txt")
Cfile = ''
with open(completeName, 'r') as fin:
	htmlfile = fin.readlines()
	Cfile = ''.join(htmlfile)


#-----------------------------------------------------------------------------------------------------------------------
#------------------------------------------extraction des def------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

string1="<def>"
string2='</def>'
string3="<br />"
pos1=Cfile.find(string1)+len(string1)
pos2=Cfile.find(string2)	
sousChaine = Cfile[pos1:pos2]
pos3=sousChaine.find(string3)
#voir s'il y a une définition pour le terme ou pas
if pos3 < 1 :
	sousChaine= "on n'a pas de définition pour ce terme "
else:
	pos3 = pos3+len(string3)+1
	sousChaine =sousChaine[pos3:]
	sousChaine =sousChaine.replace("<br />","")
save_path_def = save_path+'/def'
if not os.path.exists(save_path_def):
	os.makedirs(save_path_def)
completeNamedef = os.path.join(save_path_def,mot+".txt")
fichier = open(completeNamedef, "w")
fichier.write(sousChaine)
fichier.close()	
	#print ('écriture def')

#-----------------------------------------------------------------------------------------------------------------------
#-------------------------------------extraction des relations-------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

string1="// les types de relations (Relation Types) : rt;rtid;'trname';'trgpname';'rthelp' "
string2="// les relations sortantes : r;rid;node1;node2;type;w"
#string3="<br />"
pos1=Cfile.find(string1)+len(string1)
pos2=Cfile.find(string2)	
sousChaine = Cfile[pos1:pos2]
#pos3=sousChaine.find(string3)
#print sousChaine

majmouatrelation = sousChaine.splitlines()
# enregistrer les couples id_nomrelation id_defrelation dans un dic
for majmouatrelation2 in majmouatrelation :
	if '\n' != majmouatrelation2 :
		if 'rt;' in majmouatrelation2 :
			majmouatrelation3 = majmouatrelation2.split(';')
			relationids.append(majmouatrelation3[1])
			dicrelation[majmouatrelation3[1]] = majmouatrelation3[3]
			dicdefrelation[majmouatrelation3[1]] = majmouatrelation3[4]

#-----------------------------------------------------------------------------------------------------------------------
#-------------------------------------extraction des elements-------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

string1="// les noeuds/termes (Entries) : e;eid;'name';type;w;'formated name' "
string2="// les types de relations (Relation Types) : rt;rtid;'trname';'trgpname';'rthelp' "
#string3="<br />"
pos1=Cfile.find(string1)+len(string1)
pos2=Cfile.find(string2)	
sousChaine = Cfile[pos1:pos2]
majmoua = sousChaine.splitlines()
# enregistrer les couples id_element dans un dic
for majmoua2 in majmoua :
	if 'e;' in majmoua2 :
		#print majmoua2
		majmoua3 = majmoua2.split(';')
		#print majmoua3
		nomelement1 = str(majmoua3[2])
		nomelement1 = nomelement1.replace("'","")
		if  '>' in nomelement1:
			nomelement1 = str(majmoua3[5])
			nomelement1 = nomelement1.replace("'","")
		dicelement [majmoua3[1]]= nomelement1

#-----------------------------------------------------------------------------------------------------------------------
#------------------------------extraction ID+nom relations------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------
#final file with association + def + words
save_path_info = save_path+'/info'
if not os.path.exists(save_path_info):
	os.makedirs(save_path_info)
completeNameinfo = os.path.join(save_path_info,mot+".txt")
nomrelation=''
idrelatio=''
defrelation=''
idelement1=''
idelement2=''
weight=''
nomelement1=''
nomelement2=''
#recuperation id element rechercher

getid = htmlfile[1:2]
#print getid
getid = str(getid)
posid1= getid.find("' (eid=")+len("' (eid=")
posid2= getid.find(")")
theid= getid[posid1:posid2]
#print theid


#-----------------------------------------------------------------------------------------------------------------------
#------------------------------------------extraction des associations------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

string1="// les relations sortantes : r;rid;node1;node2;type;w "
string2='// END'
pos1=Cfile.find(string1)+len(string1)
pos2=Cfile.find(string2)	
sousChaine = Cfile[pos1:pos2]


majmouatassociation = sousChaine.splitlines()
visitedre = []
for majmouatassociation2 in majmouatassociation :
	if '\n' != majmouatassociation2 :
		if '// les relations' not in majmouatassociation2 :
			if 'r;' in majmouatassociation2 :
				majmouatassociation3 = majmouatassociation2.split(';')
				if majmouatassociation3[4] not in visitedre :
					dicrassociation[majmouatassociation3[4]] = []
					visitedre.append(majmouatassociation3[4])
				if majmouatassociation3[2] == theid : 
					dicrassociation[majmouatassociation3[4]].append(majmouatassociation3[3])
				else :
					dicrassociation[majmouatassociation3[4]].append(majmouatassociation3[2])
			
#pour chaque relation: def, element liee		

	
#liste des ids des elements visiter
visitedlist = []
#ouvrire fichier data 
lielement =""

for tawa in relationids :
	#print tawa
	visitedlist = []
	nomrelation = dicrelation[tawa]
	defrelation = dicdefrelation[tawa] 
	elementli = dicrassociation[tawa] 
	lielement =""
	for hetha in elementli :
		if hetha not in visitedlist :
			visitedlist.append(hetha)
			lielement =lielement +" "+ dicelement[hetha]
		
#print 'la relation en question est '+nomrelation+' ayant comme définition '+defrelation
#ouverture fichier des données complet
	fichier = open(completeNameinfo, "a")
	fichier.write('\n')	
	fichier.write('\n')	
	fichier.write('\n')	
	fichier.write("the association :")
	fichier.write('\n')	
	fichier.write(nomrelation)
	fichier.write('\n')	
	fichier.write("it's id :")	
	fichier.write(tawa)
	fichier.write('\n')									
	fichier.write("it's definition:")
	fichier.write('\n')	
	fichier.write(defrelation)
	fichier.write('\n')	
	fichier.write(" the words related to the searched word '"+mot+"' by this association are:")
	fichier.write(lielement)
	fichier.write('\n')
#	print ('on a toute la structure de lassociation '+nomrelation)				
fichier.close()	




























