
// DUMP pour le terme 'tu' (eid=113126)

<def>
<br />
1. Pronom personnel sujet de la deuxi�me personne du singulier. Note : Il d�signe la personne � qui l'on parle, et peut �tre aussi bien masculin que f�minin. C'est un pronom clitique ; cela signifie qu'il se trouve toujours � proximit� du verbe dont il ne peut �tre s�par� que par un autre pronom clitique ou par la particule n�gative ne, soit imm�diatement apr�s le verbe dans l'inversion clitique. Dans tous les autres cas, on emploie te ou toi comme pronom personnel de la seconde personne du singulier.<br /> 
 O� es-tu ?   <br /> 
 Tu es heureux.   <br /> 
 Il t'accordera volontiers cette faveur, encore dois-tu la m�riter.   <br /> 
 Luke, tu es mon fils.   <br /> 
 Tu m'as parl� de cette affaire.   <br /> 
 � Rom�o, Rom�o, pourquoi es-tu Rom�o ?    
 (William Shakespeare, Rom�o et Juliette)  <br /> 
 Tu ne le verras plus.   <br /> 
 O� t'en vas-tu pens�e o� t'en vas-tu rebelle.    
 (Louis Aragon)  <br /> 
 Tu ne m'as pas r�pondu, l'autre jour.   <br /> 
 Ainsi, dis-tu, il viendra ce soir ?   <br /> 
 En as-tu suffisamment ?   <br /> 
 Tu en auras des nouvelles.   <br /> 
 Peux-tu dire une telle chose!   <br /> 
 Tu y �tais.   <br /> 
 Tu t'en repentiras.  <br />
2. Pronom personnel sujet de la deuxi�me personne du singulier. Note : Il d�signe la personne � qui l'on parle, et peut �tre aussi bien masculin que f�minin. C'est un pronom clitique ; cela signifie qu'il se trouve toujours � proximit� du verbe dont il ne peut �tre s�par� que par un autre pronom clitique ou par la particule n�gative ne, soit imm�diatement apr�s le verbe dans l'inversion clitique. Dans tous les autres cas, on emploie te ou toi comme pronom personnel de la seconde personne du singulier.<br /> 
 O� t'en vas-tu pens�e o� t'en vas-tu rebelle.    
 (Louis Aragon)  <br /> 
 Tu y �tais.   <br /> 
 O� es-tu ?   <br /> 
 Tu ne m'as pas r�pondu, l'autre jour.   <br /> 
 En as-tu suffisamment ?   <br /> 
 Tu t'en repentiras.   <br /> 
 Il t'accordera volontiers cette faveur, encore dois-tu la m�riter.   <br /> 
 Tu en auras des nouvelles.   <br /> 
 � Rom�o, Rom�o, pourquoi es-tu Rom�o ?    
 (William Shakespeare, Rom�o et Juliette)  <br /> 
 Tu es heureux.   <br /> 
 Peux-tu dire une telle chose!   <br /> 
 Tu m'as parl� de cette affaire.   <br /> 
 Tu ne le verras plus.   <br /> 
 Ainsi, dis-tu, il viendra ce soir ?   <br /> 
 Luke, tu es mon fils.  
</def>



// les types de noeuds (Nodes Types) : nt;ntid;'ntname'

nt;1;'n_term'
nt;2;'n_form'
nt;4;'n_pos'
nt;6;'n_flpot'
nt;18;'n_data'
nt;36;'n_data_pot'
nt;444;'n_link'
nt;777;'n_wikipedia'

// les noeuds/termes (Entries) : e;eid;'name';type;w;'formated name' 

e;113126;'tu';1;292
e;58250;'ignorance';1;182
e;28676;'toi-m�me';1;84
e;11177;'dissimul�';1;74
e;41450;'tacite';1;56
e;113129;'d�guis�';1;54
e;231580;'Tu';1;52
e;143257;'occulte';1;78
e;1451;'couvert';1;848
e;3191499;'c�l�';1;0
e;3187357;'kasch�';1;0
e;146156;'gard�';1;96
e;3738;'voil�';1;152
e;69971;'rentr�';1;76
e;3187329;'casch�';1;0
e;43466;'pass�';1;840
e;74013;'enfoui';1;62
e;2018332;'cel�';2;0
e;3187330;'cawch�';1;0
e;3187175;'cash�';1;0
e;184632;'TU';1;12
e;153870;'supprim�';1;56
e;3187010;'kash�';1;0
e;116477;'personne';1;9700
e;2265699;'rec�l�';2;0
e;57360;'d�vor�';1;58
e;164885;'menti';1;52
e;220815;'recel�';1;50
e;105547;'omis';1;56
e;38220;'cach�';1;338
e;36267;'taire';1;150
e;152246;'chant�';1;138
e;44342;'dit';1;98
e;152779;'cri�';1;78
e;33938;'nomm�';1;114
e;139513;'phrase';1;1334
e;54490;'grammaire';1;1580
e;38078;'conjugaison';1;750
e;72845;'elles';1;270
e;140134;'te';1;96
e;47186;'elle';1;604
e;87252;'il';1;316
e;10622;'ils';1;292
e;45244;'vous';1;346
e;24637;'nous';1;378
e;15669;'je';1;302
e;158661;'pronom personnel';1;298
e;99572;'toi';1;580
e;80635;'pronom';1;560
e;239128;'_COM';36;50
e;226223;'tu>80635';1;50;'tu>pronom'
e;226224;'tu>36267';1;8;'tu>taire'
e;171869;'Adj:';4;50
e;171870;'Nom:';4;50
e;146888;'Adj:Mas+SG';4;50
e;2356740;'Pro:Pers';4;0
e;2586627;'Gender:Mas';4;0
e;2586629;'Number:Sing';4;0
e;161702;'Ver:PPas';4;50
e;2931521;'Pro:Pers:SUJ';4;0
e;150960;'Pro:SG+P2';4;50
e;212235;'Ver:';4;50
e;147826;'Pro:';4;50
e;161699;'Ver:PPas+Mas+SG';4;50
e;62274;'propag�';1;54
e;114584;'publi�';1;62
e;57111;'signal�';1;50
e;87369;'�bruit�';1;56
e;1792636;'confi�';1;0
e;416471;'proclam�';1;52
e;2196669;'exhal�';2;0
e;2109094;'relat�';2;0
e;147115;'cont�';1;56
e;17360;'r�v�l�';1;54
e;42906;'d�clar�';1;54
e;59792;'�nonc�';1;96
e;2154565;'confess�';2;0
e;2077120;'racont�';2;0
e;2117280;'retrac�';2;0
e;95661;'communiqu�';1;90
e;163449;'montr�';1;66
e;9566;'rapport�';1;54
e;2147415;'clabaud�';2;0
e;17790;'colport�';1;54
e;123585;'affirm�';1;56
e;118148;'averti';1;74
e;104947;'d�couvert';1;148
e;124610;'�crit';1;452
e;37990;'articul�';1;66
e;32667;'produit';1;514
e;1997080;'d�ball�';2;0
e;2252555;'prof�r�';2;0
e;19670;'avou�';1;68
e;1646497;'grond�';1;0
e;145322;'annonc�';1;64
e;79212;'d�nonc�';1;50
e;60032;'expos�';1;122
e;18230;'formul�';1;56
e;100426;'d�velopp�';1;100
e;1386;'divulgu�';1;58
e;76476;'mentionn�';1;54
e;64221;'consign�';1;50
e;127444;'exprim�';1;52
e;97533;'d�voil�';1;116
e;89496;'appris';1;62
e;151550;'_FL:8';6;50
e;151576;'_FL:6';6;50
e;151566;'_FL:7';6;50
e;151553;'_FL:0';6;50
e;3575927;'Morpho:min';18;50
e;3576104;'Morpho:nospace';18;50
e;157023;'Nom:Propre';18;50
e;90634;'tutoiement';1;56
e;5152;'tutoyer';1;58
e;103405;'indiff�rence';1;438
e;28453;'bonheur';1;2060
e;317329;'_POLIT_7NOPOL';36;50
e;254878;'_POL-NEG_PC';36;50
e;317323;'_POLIT_2PG';36;50
e;248198;'_SEX_NO';36;50
e;254876;'_POL-POS_PC';36;50
e;251716;'_INFO-MONOSEMIC';36;50
e;217817;'_INFO-POLYSEMIC';36;50
e;273221;'_INFO-WIKI-NO';36;50
e;2355199;'_OCC-IGNORE';36;0
e;438949;'_INFO-POLYMORPHIC';36;50
e;163012;'_INFO-NO-MORE-QUESTION';36;6094
e;223172;'_POL-NEG';36;50
e;2585997;'_INFO-CNRTL-NO';36;50
e;254877;'_POL-NEUTRE_PC';36;50
e;223173;'_POL-POS';36;50
e;241794;'_POL-NEUTRE';36;50
e;3843375;'dbnary:fra:__ws_1_tu__pronom_personnel__1';444;0
e;225917;'personne>54490';1;50;'personne>grammaire'
e;3600;'Gansu';1;54
e;112905;'rire';1;1976
e;123616;'silence';1;610
e;47647;'familier';1;184
e;302536;'R�publique de Bouriatie';777;50
e;254171;'gard� secret';1;50
e;2214914;'j'';2;0
e;5827399;'qingke jiu';777;0

// les types de relations (Relation Types) : rt;rtid;'trname';'trgpname';'rthelp' 

rt;0;'r_associated';'id�e associ�e';Il est demand� d'�num�rer les termes les plus �troitement associ�s au mot cible... Ce mot vous fait penser � quoi ?
rt;1;'r_raff_sem';'raffinement s�mantique';Raffinement s�mantique vers un usage particulier du terme source
rt;4;'r_pos';'POS';Partie du discours (Nom, Verbe, Adjectif, Adverbe, etc.)
rt;5;'r_syn';'synonyme';Il est demand� d'�num�rer les synonymes ou quasi-synonymes de ce terme.
rt;6;'r_isa';'g�n�rique';Il est demand� d'�num�rer les GENERIQUES/hyperonymes du terme. Par exemple, 'animal' et 'mammif�re' sont des g�n�riques de 'chat'.
rt;7;'r_anto';'contraire';Il est demand� d'�num�rer des contraires du terme. Par exemple, 'chaud' est le contraire de 'froid'.
rt;8;'r_hypo';'sp�cifique';Il est demand� d'�num�rer des SPECIFIQUES/hyponymes du terme. Par exemple, 'mouche', 'abeille', 'gu�pe' pour 'insecte'.
rt;12;'r_flpot';' 	r_flpot';(relation interne) potentiel de relation
rt;13;'r_agent';'action>agent';L'agent (qu'on appelle aussi le sujet) est l'entit� qui effectue l'action, OU la subit pour des formes passives ou des verbes d'�tat. Par exemple, dans - Le chat mange la souris -, l'agent est le chat. Des agents typiques de 'courir' peuvent �tre 'sportif', 'enfant',...
rt;18;'r_data';'r_data';Informations diverses (plut�t d'ordre lexicales)
rt;19;'r_lemma';'r_lemma';Le lemme (par exemple 'mangent a pour lemme  'manger' ; 'avions' a pour lemme 'avion' ou 'avoir').
rt;22;'r_family';'famille';Des mots de la m�me famille lexicale sont demand�s (d�rivation morphologique, par exemple). Par exemple, pour 'lait' on pourrait mettre 'laitier', 'laitage', 'laiterie', etc.
rt;32;'r_sentiment';'sentiment';Pour un terme donn�, �voquer des mots li�s � des SENTIMENTS ou des EMOTIONS que vous pourriez associer � ce terme. Par exemple, la joie, le plaisir, le d�go�t, la peur, la haine, l'amour, l'indiff�rence, l'envie, avoir peur, horrible, etc.
rt;35;'r_meaning';'sens/signification';Quels SENS/SIGNIFICATIONS pouvez vous donner au terme propos�. Il s'agira de termes �voquant chacun des sens possibles, par exemple : 'forces de l'ordre', 'contrat d'assurance', 'police typographique', ... pour 'police'.
rt;36;'r_infopot';'information potentielle';Information s�mantique potentielle
rt;77;'r_verb_ppas';'r_verb_ppas';Le participe pass� (au masculin singulier) du verbe infinitif. Par exemple, pour manger => mang�
rt;444;'r_link';'r_link';Lien vers une ressource externe (WordNet, RadLex, UMLS, Wikipedia, etc...)
rt;777;'r_wiki';'r_wiki';Associations issues de wikipedia...

// les relations sortantes : r;rid;node1;node2;type;w 

r;1461046;113126;58250;0;15
r;1030095;113126;28676;0;21
r;12219821;113126;11177;0;21
r;12352014;113126;41450;0;21
r;85648736;113126;113129;0;21
r;14467075;113126;231580;0;22
r;97319596;113126;143257;0;22
r;85648740;113126;1451;0;23
r;85648731;113126;3191499;0;24
r;85922635;113126;3187357;0;24
r;12219820;113126;146156;0;26
r;85648737;113126;3738;0;26
r;85648739;113126;69971;0;26
r;85922661;113126;3187329;0;26
r;12219819;113126;43466;0;27
r;12219823;113126;74013;0;27
r;85648732;113126;2018332;0;27
r;85922681;113126;3187330;0;27
r;85922769;113126;3187175;0;27
r;14473454;113126;184632;0;28
r;85648733;113126;153870;0;29
r;85922825;113126;3187010;0;29
r;1350618;113126;116477;0;30
r;85648734;113126;2265699;0;30
r;85648735;113126;57360;0;30
r;85648738;113126;164885;0;30
r;85922650;113126;220815;0;30
r;1652122;113126;105547;0;70
r;1652121;113126;38220;0;140
r;1652116;113126;36267;0;150
r;1652117;113126;152246;0;150
r;1652118;113126;44342;0;150
r;1652119;113126;152779;0;150
r;1652120;113126;33938;0;150
r;1617181;113126;139513;0;160
r;1617183;113126;54490;0;170
r;1617182;113126;38078;0;180
r;147598;113126;72845;0;190
r;147390;113126;140134;0;220
r;147600;113126;47186;0;220
r;147608;113126;87252;0;220
r;147602;113126;10622;0;230
r;147604;113126;45244;0;230
r;147606;113126;24637;0;230
r;147609;113126;15669;0;230
r;1616752;113126;158661;0;280
r;147389;113126;99572;0;310
r;147388;113126;80635;0;361
r;24088528;113126;239128;0;1000000
r;1723689;113126;226223;1;41
r;1723692;113126;226224;1;41
r;1426643;113126;171869;4;-59
r;1426642;113126;171870;4;-54
r;105171576;113126;146888;4;27
r;48898700;113126;2356740;4;31
r;105208065;113126;2586627;4;35
r;105228784;113126;2586629;4;36
r;2249879;113126;161702;4;39
r;71879358;113126;2931521;4;42
r;58834;113126;150960;4;53
r;12112265;113126;212235;4;55
r;12112268;113126;147826;4;59
r;355600;113126;161699;4;60
r;12199237;113126;41450;5;-29
r;85593329;113126;153870;5;26
r;85593332;113126;113129;5;26
r;85593335;113126;69971;5;26
r;85653612;113126;3187357;5;26
r;97319597;113126;143257;5;26
r;85593331;113126;57360;5;27
r;85593333;113126;3738;5;27
r;85593336;113126;1451;5;27
r;85593334;113126;164885;5;28
r;722190;113126;74013;5;29
r;85654028;113126;3187329;5;29
r;85593328;113126;2018332;5;30
r;85653830;113126;220815;5;30
r;85666057;113126;3187010;5;30
r;85654069;113126;3187330;5;31
r;722192;113126;11177;5;32
r;722193;113126;146156;5;32
r;85593330;113126;2265699;5;34
r;85660671;113126;3187175;5;34
r;722189;113126;38220;5;35
r;722191;113126;105547;5;35
r;85593327;113126;3191499;5;37
r;722194;113126;43466;5;40
r;9902287;113126;80635;6;13
r;394391;113126;45244;7;-90
r;366736;113126;72845;7;-60
r;366740;113126;24637;7;-60
r;366742;113126;47186;7;-60
r;366744;113126;15669;7;-60
r;366746;113126;87252;7;-60
r;366738;113126;10622;7;-55
r;85593341;113126;62274;7;21
r;85593342;113126;114584;7;21
r;85593350;113126;57111;7;21
r;85593358;113126;87369;7;21
r;85593364;113126;1792636;7;21
r;85593368;113126;416471;7;21
r;85593373;113126;2196669;7;21
r;85593376;113126;2109094;7;21
r;85593353;113126;147115;7;22
r;85593361;113126;17360;7;22
r;85593365;113126;42906;7;22
r;85593366;113126;59792;7;22
r;85593370;113126;2154565;7;22
r;85593375;113126;2077120;7;22
r;85593377;113126;2117280;7;22
r;85593338;113126;95661;7;23
r;85593340;113126;163449;7;23
r;85593343;113126;9566;7;23
r;85593346;113126;2147415;7;23
r;85593347;113126;17790;7;23
r;85593351;113126;123585;7;23
r;85593352;113126;118148;7;23
r;85593354;113126;104947;7;23
r;85593359;113126;124610;7;23
r;85593363;113126;37990;7;23
r;85593369;113126;32667;7;23
r;85593372;113126;1997080;7;23
r;85593374;113126;2252555;7;23
r;85593337;113126;19670;7;24
r;85593339;113126;1646497;7;24
r;85593344;113126;145322;7;24
r;85593345;113126;79212;7;24
r;85593348;113126;60032;7;24
r;85593349;113126;18230;7;24
r;85593355;113126;100426;7;24
r;85593357;113126;1386;7;24
r;85593360;113126;76476;7;24
r;85593371;113126;64221;7;24
r;85593367;113126;127444;7;27
r;85593356;113126;97533;7;29
r;85593362;113126;89496;7;29
r;373952;113126;33938;7;51
r;373948;113126;152246;7;54
r;373950;113126;152779;7;54
r;370987;113126;44342;7;80
r;383194;113126;151550;12;13
r;383195;113126;151576;12;22
r;258318;113126;151566;12;27
r;63869;113126;151553;12;70
r;89987952;113126;3575927;18;29
r;90848883;113126;3576104;18;29
r;78667635;113126;157023;18;30
r;339682;113126;113126;19;54
r;339669;113126;36267;19;57
r;97319593;113126;90634;22;15
r;97319594;113126;5152;22;24
r;102647437;113126;103405;32;9
r;97319595;113126;28453;32;21
r;25055463;113126;36267;35;42
r;25055462;113126;80635;35;45
r;8930606;113126;317329;36;-1
r;27250621;113126;254878;36;6
r;8930605;113126;317323;36;16
r;2190866;113126;248198;36;17
r;2360270;113126;254876;36;19
r;96012651;113126;251716;36;28
r;1723717;113126;217817;36;40
r;29774476;113126;273221;36;40
r;48898909;113126;2355199;36;42
r;18566194;113126;438949;36;44
r;1576834;113126;163012;36;54
r;26513967;113126;223172;36;54
r;54877894;113126;2585997;36;59
r;2360271;113126;254877;36;75
r;2009325;113126;223173;36;169
r;2009370;113126;241794;36;680
r;94236510;113126;3843375;444;1

// les relations entrantes : r;rid;node1;node2;type;w 

r;258317;33938;113126;7;60
r;428386;47186;113126;0;180
r;366332;158661;113126;0;120
r;361256;140134;113126;0;70
r;1838054;225917;113126;0;45
r;2142328;5152;113126;0;70
r;4180444;3600;113126;777;10
r;154796;99572;113126;0;260
r;8552891;112905;113126;13;-14
r;8296258;123616;113126;0;17
r;63868;80635;113126;0;91
r;432598;15669;113126;0;410
r;383193;80635;113126;8;111
r;2173118;116477;113126;0;28
r;21121343;58250;113126;0;35
r;1684451;47647;113126;0;53
r;1023122;28676;113126;0;40
r;72845634;302536;113126;777;10
r;72677939;36267;113126;0;161
r;77359550;36267;113126;77;45
r;1723685;41450;113126;5;-30
r;339682;113126;113126;19;54
r;1661251;24637;113126;0;102
r;12199236;41450;113126;0;26
r;12222703;105547;113126;0;27
r;12268323;146156;113126;0;30
r;12306350;43466;113126;0;23
r;12308636;38220;113126;0;23
r;12348746;11177;113126;0;27
r;12235597;74013;113126;0;23
r;14467074;231580;113126;0;23
r;14473453;184632;113126;0;26
r;97859906;143257;113126;5;29
r;85649939;164885;113126;0;27
r;85652535;3191499;113126;0;30
r;85653611;3187357;113126;0;24
r;85653829;220815;113126;0;30
r;85654027;3187329;113126;0;30
r;85654068;3187330;113126;0;21
r;85660670;3187175;113126;0;29
r;85666056;3187010;113126;0;26
r;85673619;2018332;113126;0;22
r;85681562;2265699;113126;0;26
r;85681942;57360;113126;0;29
r;85684619;153870;113126;0;30
r;85691673;1451;113126;0;27
r;85705102;113129;113126;0;21
r;98394022;143257;113126;0;26
r;1038915;45244;113126;0;32
r;85690138;69971;113126;0;26
r;85703632;3738;113126;0;31
r;99130483;254171;113126;0;61
r;72063451;2214914;113126;0;332
r;113541647;5827399;113126;777;10
r;85518205;19670;113126;7;21
r;85519702;95661;113126;7;21
r;85521830;104947;113126;7;22
r;85524324;113129;113126;5;27
r;85526237;118148;113126;7;21
r;663583;146156;113126;5;39
r;85526701;3738;113126;5;26
r;85527740;60032;113126;7;24
r;85533033;57111;113126;7;24
r;85534792;9566;113126;7;21
r;12219822;11177;113126;5;32
r;85537570;147115;113126;7;28
r;85540757;37990;113126;7;23
r;85542582;1451;113126;5;26
r;563779;43466;113126;5;31
r;85544712;69971;113126;5;26
r;85546505;1792636;113126;7;21
r;85547058;89496;113126;7;23
r;2443635;44342;113126;7;59
r;85551000;153870;113126;5;34
r;85553236;64221;113126;7;24
r;85554486;57360;113126;5;26
r;85554552;123585;113126;7;24
r;85556585;1386;113126;7;24
r;560395;38220;113126;5;37
r;85557606;152779;113126;7;22
r;88760879;2077120;113126;7;22
r;85559938;163449;113126;7;23
r;85560246;62274;113126;7;22
r;88760904;76476;113126;7;22
r;88760914;152246;113126;7;24
r;85565705;1646497;113126;7;29
r;85566687;18230;113126;7;23
r;85567199;416471;113126;7;21
r;88760924;2117280;113126;7;23
r;85568772;145322;113126;7;23
r;88760934;2109094;113126;7;22
r;85572314;97533;113126;7;22
r;85573288;114584;113126;7;23
r;85575847;2147415;113126;7;21
r;85578357;2154565;113126;7;23
r;85578473;17790;113126;7;26
r;85579676;127444;113126;7;24
r;85586291;2196669;113126;7;21
r;85591594;164885;113126;5;31
r;85592155;32667;113126;7;22
r;704206;74013;113126;5;28
r;718907;105547;113126;5;32
r;85564110;2018332;113126;5;28
r;85588299;3191499;113126;5;28
r;85572616;3187010;113126;5;32
r;85579496;3187175;113126;5;27
r;85587172;3187329;113126;5;28
r;85587193;3187330;113126;5;26
r;85587372;3187357;113126;5;28
r;85586976;220815;113126;5;30
r;85554555;2265699;113126;5;27

// END
